import sys
import cv2
import av
import numpy as np
from queue import Queue
import time
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import QTimer, QThread, pyqtSignal, pyqtSlot
from PyQt5 import QtWidgets, QtCore, QtGui

# from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QVBoxLayout
from PyQt5.QtWidgets import *
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *

from models.inf_utils_new import Inference
from capture.rawcapture_utils_new import yuv422torgb
from gui.cv_utils_new import overlay

# https://ru.stackoverflow.com/a/1150993/396441

from argparse import ArgumentParser, SUPPRESS


class AVReadAndInfer(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cam_id = args[1]
        self.width = args[2]
        self.height = args[3]
        model_name = kwargs["model_name"]
        self.fps = 30.0
        self.pix_fmt = "yuyv422"
        self.frame_counter = 0
        self.container = self._initialize_container()
        self.inf = Inference(model_name)

    def _get_ffmpeg_fmt(self):
        platform = sys.platform.lower()
        if "darwin" in platform:
            return "avfoundation"
        elif "win" in platform:
            return "dshow"
        else:
            err_msg = "%s capture not implemented" % platform
            raise NotImplementedError(err_msg)
        return None

    def _get_ffmpeg_options(self):
        options = {
            "video_size": "%sx%s" % (self.width, self.height),
            "framerate": "%s" % float(self.fps),
            "pixel_format": self.pix_fmt,
            "preset": "ultrafast",
            "probesize": "42M",
        }
        return options

    def _initialize_container(self):
        self.ffmpeg_fmt = self._get_ffmpeg_fmt()
        options = self._get_ffmpeg_options()
        self.msleep(10)
        container = av.open(
            format=self.ffmpeg_fmt, file="%s" % self.cam_id, options=options
        )

        return container

    def read_from_container(self):
        success = False
        frame = None
        stream = self.container.streams[0]
        while not success:
            try:
                for packet in self.container.demux(stream):
                    for frame in packet.decode():
                        # arr = frame.to_ndarray()
                        success = True
            except:
                pass
        return success, frame

    def close(self):
        self.container.close()

    def run(self):
        while True:
            ret1, yuv_av = self.read_from_container()
            if ret1:
                yuv_arr = yuv_av.to_ndarray()
                pred_mask = self.inf(yuv_arr)
                # rgb_arr = yuv_av.to_rgb().to_ndarray()
                rgb_arr = yuv422torgb(yuv_arr)
                disp_img = overlay(pred_mask, rgb_arr)
                height1, width1, channel1 = disp_img.shape
                step1 = channel1 * width1
                qImg1 = QImage(
                    disp_img.data, width1, height1, step1, QImage.Format_RGB888
                )
                self.changePixmap.emit(qImg1)


class AVWriteThread(QThread):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cam_id = args[1]
        self.width = args[2]
        self.height = args[3]
        self.fps = 30
        self.pix_fmt = "yuyv422"
        self.frame_counter = 0
        self.active = True
        self.container = self._initialize_container()
        self.save_name = None

    def _get_ffmpeg_fmt(self):
        platform = sys.platform.lower()
        if "darwin" in platform:
            return "avfoundation"
        elif "win" in platform:
            return "dshow"
        else:
            err_msg = "capturing in {} not implemented".format(platform)
            raise NotImplementedError(err_msg)
        return None

    def _get_ffmpeg_options(self):
        options = {
            "video_size": "%sx%s" % (self.width, self.height),
            "framerate": "%s" % float(self.fps),
            "pixel_format": self.pix_fmt,
            "preset": "ultrafast",
            "probesize": "100M",
        }
        return options

    def _initialize_container(self):
        self.ffmpeg_fmt = self._get_ffmpeg_fmt()
        options = self._get_ffmpeg_options()
        container = av.open(
            format=self.ffmpeg_fmt, file="%s" % self.cam_id, options=options
        )
        self.msleep(100)
        return container

    def read_from_container(self):
        success = False
        frame = None
        stream = self.container.streams[0]
        while not success:
            try:
                for packet in self.container.demux(stream):
                    for frame in packet.decode():
                        # arr = frame.to_ndarray()
                        success = True
            except:
                pass
        return success, frame

    def update_save_name(self, save_name):
        self.save_name = save_name

    def run(self):
        if self.active:
            timestamp = time.strftime("%d-%b-%Y-%H_%M_%S")
            save_name = (
                "/Users/harisjabra/Documents/repos/raw-capture/python_capture/%s.yuv"
                % timestamp
            )
            self.update_save_name(save_name)
            write_container = av.open(save_name, "w")
            # self.msleep(10)
            write_stream = write_container.add_stream(
                "rawvideo", rate=self.fps)
            write_stream.pix_fmt = self.pix_fmt
            # write_stream.pix_fmt = 'uyvy422'
            write_stream.width = self.width
            write_stream.height = self.height
            while self.active:
                ret1, image1 = self.read_from_container()
                if ret1:
                    # image1 = image1.reformat('uyvy422')
                    # print(image1.format)
                    write_container.mux(write_stream.encode(image1))
                # self.msleep(10)
            write_container.mux(write_stream.encode())  # flush stream
            # self.msleep(10)
            write_container.close()
            # self.msleep(3000)
            # self.container.close()


class MainWindow2(QWidget):
    def __init__(
        self, cam_id, cam_width, cam_height, cam_fps, model_name, device_name, save_dir
    ):
        super().__init__()
        self._initWindow()
        # self.setGeometry(100, 100, 800, 600)
        # self.resize(660, 520)
        self.record_start_name = "RECORD YUYV422"
        self.record_stop_name = "STOP RECORDING"
        self.control_bt = QPushButton(self.record_start_name)
        self.control_bt.clicked.connect(self.controlTimer)
        self.image_label = QLabel()
        self.saveTimer = QTimer()
        # self.th1 = Thread1(self)
        self.disp_w, self.disp_h = cam_width, cam_height
        self.cam_id = cam_id
        model_name = "/Users/harisjabra/Documents/repos/single-sensor-seg/people_seg/saved_models/gru_model.xml"
        self.th1 = AVReadAndInfer(
            self, self.cam_id, self.disp_w, self.disp_h, model_name=model_name
        )
        self.th1.changePixmap.connect(self.setImage)
        self.th1.start()

        vlayout = QVBoxLayout(self)
        vlayout.addWidget(self.image_label)
        vlayout.addWidget(self.control_bt)

    def _initWindow(self):
        self.setWindowTitle("YUV422 Data Recorder")
        left = 180
        top = 150
        self.win_height = 720
        self.win_width = 1280
        # # setting  the fixed height of window
        # self.setFixedHeight(height)
        # self.setFixedWidth(width)
        # self.center()
        self.setGeometry(left, top, self.win_width, self.win_height)
        self.show()

    @QtCore.pyqtSlot(QImage)
    def setImage(self, qImg1):
        qImg1 = qImg1.scaled(self.win_width, self.win_height)
        self.image_label.setPixmap(QPixmap.fromImage(qImg1))

    def controlTimer(self):
        if not self.saveTimer.isActive():
            # write video
            self.saveTimer.start()
            self.th2 = AVWriteThread(
                self, self.cam_id, self.disp_w, self.disp_h)
            self.th2.active = True
            self.th2.start()
            # update control_bt text
            self.control_bt.setText(self.record_stop_name)
        else:
            # stop writing
            self.saveTimer.stop()
            print("Recording saved in:")
            print(self.th2.save_name)
            self.th2.active = False
            # self.th2.stop()
            # self.th2.terminate()
            # update control_bt text
            self.control_bt.setText(self.record_start_name)


class MainWindow(QMainWindow):

    # constructor
    def __init__(self):
        super().__init__()

        # setting geometry
        self.setGeometry(100, 100, 800, 600)

        # setting style sheet
        self.setStyleSheet("background : lightgrey;")

        # getting available cameras
        self.available_cameras = QCameraInfo.availableCameras()

        # if no camera found
        if not self.available_cameras:
            # exit the code
            sys.exit()

        # creating a status bar
        self.status = QStatusBar()

        # setting style sheet to the status bar
        self.status.setStyleSheet("background : white;")

        # adding status bar to the main window
        self.setStatusBar(self.status)

        # path to save
        self.save_path = ""

        # creating a QCameraViewfinder object
        self.viewfinder = QCameraViewfinder()

        # showing this viewfinder
        self.viewfinder.show()

        # making it central widget of main window
        self.setCentralWidget(self.viewfinder)

        # Set the default camera.
        self.select_camera(0)

        # creating a tool bar
        toolbar = QToolBar("Settings Tool Bar")

        # adding tool bar to main window
        self.addToolBar(toolbar)

        # creating a photo action to take photo
        click_action = QAction("Click photo", self)

        # adding status tip to the photo action
        click_action.setStatusTip("This will capture picture")

        # adding tool tip
        click_action.setToolTip("Capture picture")

        # adding action to it
        # calling take_photo method
        click_action.triggered.connect(self.click_photo)

        # adding this to the tool bar
        toolbar.addAction(click_action)

        # Create Layout
        vlayout = QVBoxLayout(self)
        # create record start/stop button
        self.record_start_name = "RECORD YUYV422"
        self.record_stop_name = "STOP RECORDING"
        control_bt = QPushButton(self)
        vlayout.addWidget(control_bt)
        # self.addDockWidget(control_bt)
        # # adding button to the tool bar
        # toolbar.addAction(control_bt)

        # similarly creating action for changing save folder
        change_folder_action = QAction("Change save location", self)

        # adding status tip
        change_folder_action.setStatusTip(
            "Change folder where picture will be saved saved."
        )

        # adding tool tip to it
        change_folder_action.setToolTip("Change save location")

        # setting calling method to the change folder action
        # when triggered signal is emitted
        change_folder_action.triggered.connect(self.change_folder)

        # adding this to the tool bar
        toolbar.addAction(change_folder_action)

        # creating a combo box for selecting camera
        camera_selector = QComboBox()

        # adding status tip to it
        camera_selector.setStatusTip("Choose camera to take pictures")

        # adding tool tip to it
        camera_selector.setToolTip("Select Camera")
        camera_selector.setToolTipDuration(2500)

        # adding items to the combo box
        camera_selector.addItems(
            [camera.description() for camera in self.available_cameras]
        )

        # adding action to the combo box
        # calling the select camera method
        camera_selector.currentIndexChanged.connect(self.select_camera)

        # adding this to tool bar
        toolbar.addWidget(camera_selector)

        # setting tool bar stylesheet
        toolbar.setStyleSheet("background : white;")

        # setting window title
        self.setWindowTitle("PyQt5 Cam")

        # showing the main window
        self.show()

    # method to select camera
    def select_camera(self, i):

        # getting the selected camera
        self.camera = QCamera(self.available_cameras[i])

        # setting view finder to the camera
        self.camera.setViewfinder(self.viewfinder)

        # setting capture mode to the camera
        self.camera.setCaptureMode(QCamera.CaptureStillImage)

        # if any error occur show the alert
        self.camera.error.connect(
            lambda: self.alert(self.camera.errorString()))

        # start the camera
        self.camera.start()

        # creating a QCameraImageCapture object
        self.capture = QCameraImageCapture(self.camera)

        # showing alert if error occur
        self.capture.error.connect(
            lambda error_msg, error, msg: self.alert(msg))

        # when image captured showing message
        self.capture.imageCaptured.connect(
            lambda d, i: self.status.showMessage(
                "Image captured : " + str(self.save_seq)
            )
        )

        # getting current camera name
        self.current_camera_name = self.available_cameras[i].description()

        # inital save sequence
        self.save_seq = 0

    # method to take photo
    def click_photo(self):

        # time stamp
        timestamp = time.strftime("%d-%b-%Y-%H_%M_%S")

        # capture the image and save it on the save path
        self.capture.capture(
            os.path.join(
                self.save_path,
                "%s-%04d-%s.jpg" % (self.current_camera_name,
                                    self.save_seq, timestamp),
            )
        )

        # increment the sequence
        self.save_seq += 1

    # change folder method
    def change_folder(self):

        # open the dialog to select path
        path = QFileDialog.getExistingDirectory(self, "Picture Location", "")

        # if path is selected
        if path:

            # update the path
            self.save_path = path

            # update the sequence
            self.save_seq = 0

    # method for alerts
    def alert(self, msg):

        # error message
        error = QErrorMessage(self)

        # setting text to the error message
        error.showMessage(msg)


def build_argparser():
    """Defines input argurments

    Returns:
        parser: contains all arguments
    """
    parser = ArgumentParser(add_help=False)
    args = parser.add_argument_group("Options")
    args.add_argument(
        "-h",
        "--help",
        action="help",
        default=SUPPRESS,
        help="Show this help message and exit.",
    )
    args.add_argument(
        "-i",
        "--cam_id",
        required=True,
        help="Webcamera ID",
    )
    args.add_argument(
        "-d",
        "--device",
        type=str,
        default="CPU",
        help="One of two strings: CPU, MYRIAD",
    )
    args.add_argument("-s", "--save_dir", type=str,
                      default="", help="save location")
    args.add_argument(
        "-m",
        "--model",
        type=str,
        default="saved_models/gru_model.xml",
        help="Path to model file (xml or blob)",
    )
    args.add_argument(
        "-wd", "--width", type=int, default=1280, help="width of video capture"
    )
    args.add_argument(
        "-ht", "--height", type=int, default=720, help="height of video capture"
    )

    return parser


if __name__ == "__main__":
    args = build_argparser().parse_args()
    cam_id = args.cam_id
    model_name = args.model
    device_name = args.device
    save_dir = args.save_dir
    cam_width, cam_height = args.width, args.height
    cam_fps = 30.0
    app = QApplication(sys.argv)
    mainWindow = MainWindow2(
        cam_id, cam_width, cam_height, cam_fps, model_name, device_name, save_dir
    )
    mainWindow.show()
    sys.exit(app.exec_())

    # Run this for converting to mp4 video
    # "ffmpeg -f rawvideo -pixel_format yuyv422 -video_size 1280x720 -i "02-Dec-2021-13_38_27.yuv" -c:v libx264rgb -crf 25 -s 1280x720 converted.mp4"
