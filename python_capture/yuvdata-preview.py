import sys
import time
import cv2
import av
import numpy as np

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QVBoxLayout
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import QTimer, QThread, pyqtSignal, pyqtSlot
from PyQt5 import QtWidgets, QtCore, QtGui

# https://ru.stackoverflow.com/a/1150993/396441


def yuv422torgb(yuv422_frame):
    Y = yuv422_frame[:, :, 0:1]
    U = yuv422_frame[:, 0::2, 1:2]
    V = yuv422_frame[:, 1::2, 1:2]
    U_full = np.zeros_like(Y)
    V_full = np.zeros_like(Y)
    # repeat to match size
    U_full[:, 0::2, :] = U
    U_full[:, 1::2, :] = U
    V_full[:, 0::2, :] = V
    V_full[:, 1::2, :] = V
    V = V_full
    U = U_full
    # YUV = np.dstack((Y, U, V))[:height, :width, :].astype(np.float)
    YUV = np.dstack((Y, U, V)).astype(np.float)
    YUV[:, :, 0] = YUV[:, :, 0] - 16  # Offset Y by 16
    YUV[:, :, 1:] = YUV[:, :, 1:] - 128  # Offset UV by 128
    # YUV conversion matrix from ITU-R BT.601 version (SDTV)
    #              Y       U       V
    M_SD = np.array(
        [
            [1.164, 0.000, 1.596],  # R
            [1.164, -0.392, -0.813],  # G
            [1.164, 2.017, 0.000],
        ]
    )  # B

    M_HD = np.array([[1.164, 0.0, 1.793], [1.164, -0.213, -0.533], [1.164, 2.112, 0.0]])
    # Take the dot product with the matrix to produce RGB output, clamp the
    # results to byte range and convert to bytes
    RGB = YUV.dot(M_HD.T).clip(0, 255).astype(np.uint8)
    return RGB


class AVCapture:
    def __init__(self, cam_id=3, width=1280, height=720, fps=30):
        super().__init__()
        self.cam_id = cam_id
        self.width = width
        self.height = height
        self.fps = fps
        self.pix_fmt = "yuyv422"
        self.ffmpeg_fmt = self._get_ffmpeg_fmt()
        self.container = self._initialize_container()
        self.frame_counter = 0

    def _get_ffmpeg_fmt(self):
        platform = sys.platform.lower()
        if "darwin" in platform:
            return "avfoundation"
        elif "win" in platform:
            return "dshow"
        else:
            err_msg = "capturing in {} not implemented".format(platform)
            raise NotImplementedError(err_msg)
        return None

    def _get_ffmpeg_options(self):
        options = {
            "video_size": "%sx%s" % (self.width, self.height),
            "framerate": "%s" % float(self.fps),
            "pixel_format": self.pix_fmt,
        }
        return options

    def _initialize_container(self):
        options = self._get_ffmpeg_options()
        container = av.open(
            format=self.ffmpeg_fmt, file="%s" % self.cam_id, options=options
        )
        return container

    def read(self):
        success = False
        frame = None
        stream = self.container.streams[0]
        while not success:
            try:
                for packet in self.container.demux(stream):
                    for frame in packet.decode():
                        success = True
            except:
                pass
        return success, frame

    def read_rgb(self):
        success = False
        frame = None
        stream = self.container.streams[0]
        while not success:
            try:
                for packet in self.container.demux(stream):
                    for frame in packet.decode():
                        arr = frame.to_rgb().to_ndarray()
                        success = True
            except:
                pass
        return success, arr

    def close(self):
        self.container.close()


class AVReadInferThread1(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.cap = AVCapture()

    def run(self):
        while True:
            ret1, image1 = self.cap.read()
            if ret1:
                im1 = image1.to_ndarray()
                im1 = yuv422torgb(im1)
                height1, width1, channel1 = im1.shape
                step1 = channel1 * width1
                qImg1 = QImage(im1.data, width1, height1, step1, QImage.Format_RGB888)
                self.changePixmap.emit(qImg1)


class AVReadWriteThread(QThread):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.read_cap = AVCapture()
        self.make_write_cap()

    def make_write_cap(self, path=None):
        if path is None:
            time_stamp = time.strftime("%d%b%y-%H_%M_%S")
            path = time_stamp + ".yuv"
        self.container = av.open(path, mode="w")
        fps = 30.0
        width = 1280
        height = 720
        self.stream = self.container.add_stream("h264", rate=round(fps))
        self.stream.width = width
        self.stream.height = height

    def run(self):
        if self.active:
            while self.active:
                ret1, image1 = self.read_cap.read()
                if ret1:
                    self.container.mux(self.stream.encode(image1))
                self.msleep(10)

    def stop(self):
        self.container.mux(self.stream.encode())
        self.container.close()


class Thread2(QThread):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.active = True

    def run(self):
        if self.active:
            self.fourcc = cv2.VideoWriter_fourcc(*"XVID")
            self.out1 = cv2.VideoWriter("output.avi", self.fourcc, 30, (640, 480))
            self.cap1 = cv2.VideoCapture(3, cv2.CAP_AVFOUNDATION)
            self.cap1.set(3, 480)
            self.cap1.set(4, 640)
            self.cap1.set(5, 30)
            while self.active:
                ret1, image1 = self.cap1.read()
                if ret1:
                    self.out1.write(image1)
                self.msleep(10)

    def stop(self):
        self.out1.release()


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.resize(660, 520)
        self.control_bt = QPushButton("RECORD YUYV422")
        self.control_bt.clicked.connect(self.controlTimer)
        self.image_label = QLabel()
        self.saveTimer = QTimer()
        self.th1 = AVReadInferThread1(self)
        self.th1.changePixmap.connect(self.setImage)
        self.th1.start()

        vlayout = QVBoxLayout(self)
        vlayout.addWidget(self.image_label)
        vlayout.addWidget(self.control_bt)

    @QtCore.pyqtSlot(QImage)
    def setImage(self, qImg1):
        self.image_label.setPixmap(QPixmap.fromImage(qImg1))

    def controlTimer(self):
        if not self.saveTimer.isActive():
            # write video
            self.saveTimer.start()
            self.th2 = Thread2(self)
            self.th2.active = True
            self.th2.start()
            # update control_bt text
            self.control_bt.setText("STOP")
        else:
            # stop writing
            self.saveTimer.stop()
            self.th2.active = False
            self.th2.stop()
            self.th2.terminate()
            # update control_bt text
            self.control_bt.setText("RECORD YUYV422")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())
