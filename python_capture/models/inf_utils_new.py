import cv2, numpy as np
from openvino.inference_engine import IECore


class ProcessYUYVInput(object):
    def __init__(self, width, height, channels=2):
        self.width = width
        self.height = height
        self.dn_inter = cv2.INTER_AREA
        self.up_inter = cv2.INTER_LINEAR

    def resize_yuv_img(self, yuv_img, resize_wh):
        orig_h, orig_w, orig_nch = yuv_img.shape
        y = cv2.resize((yuv_img[:, :, 0]), resize_wh, interpolation=(self.dn_inter))
        u = cv2.resize(
            (yuv_img[:, 0::2, 1]),
            (resize_wh[0] // 2, resize_wh[1]),
            interpolation=(self.dn_inter),
        )
        v = cv2.resize(
            (yuv_img[:, 1::2, 1]),
            (resize_wh[0] // 2, resize_wh[1]),
            interpolation=(self.dn_inter),
        )
        ret_img = np.zeros((resize_wh[1], resize_wh[0], orig_nch), dtype=(y.dtype))
        ret_img[:, :, 0] = y
        ret_img[:, 0::2, 1] = u
        ret_img[:, 1::2, 1] = v
        return ret_img

    def yuyv_to_uyvy(self, yuyv_img):
        uyvy = yuyv_img[:, :, [1, 0]]
        return uyvy

    def ToMXformat(self, I):
        if len(I.shape) == 2:
            I = np.expand_dims(I, axis=(0, 1))
        else:
            if len(I.shape) == 3:
                I = np.expand_dims(I, axis=0)
                I = np.transpose(I, (0, 3, 1, 2))
            else:
                raise ValueError("Unsupported input shape format: ", I.shape)
        I = I / 255
        return I

    def proces_img(self, yuyv_img):
        resize_wh = (self.width, self.height)
        yuyv_img = self.resize_yuv_img(yuyv_img, resize_wh)
        uyvy_img = self.yuyv_to_uyvy(yuyv_img)
        in_img = self.ToMXformat(uyvy_img)
        return in_img

    def __call__(self, img):
        img = self.proces_img(img)
        return img


class ProcessOutput(object):
    def __init__(self, intp_type=cv2.INTER_LINEAR):
        self.inter = intp_type

    def resize(self, img, resize_shape):
        img = cv2.resize(img, resize_shape, self.inter)
        return img

    def overlay(self, mask, in_img):
        alpha = np.expand_dims(mask, axis=(-1))
        overlapped = mask * in_img + (1 - alpha) * in_img
        overlapped = np.clip(overlapped, 0, 255)
        overlapped = np.uint8(overlapped)
        return overlapped

    def process_output_mask(self, pred_mask, in_img):
        if len(pred_mask.shape) == 4:
            pred_mask = np.transpose(pred_mask[0], (1, 2, 0))
        else:
            raise NotImplementedError("Unsupported model output shape", pred_mask.shape)
        height, width = in_img.shape[0], in_img.shape[1]
        pred_mask = self.resize(pred_mask, (width, height))
        return pred_mask

    def __call__(self, pred_mask, in_img):
        pred_mask = pred_mask.astype(np.float32)
        pred_mask = self.process_output_mask(pred_mask, in_img)
        return pred_mask


class Inference:
    def __init__(self, model_name):
        self.model_name = model_name
        self.device_name = "CPU"
        self.Model = self._get_model()
        self._initialize_model_inputs()
        self._initialize_model_outputs()
        self.pre_procesor = self._get_preprocessor()
        self.post_processor = self._get_postprocessor()

    def _get_model(self):
        ie = IECore()
        print("Loading network")
        if ".xml" in self.model_name:
            bin_name = self.model_name.replace(".xml", ".bin")
            net = ie.read_network(self.model_name, bin_name)
            print("Loading model to the plugin")
            if self.device_name == "MYRIAD":
                config = {
                    "MYRIAD_NUMBER_OF_SHAVES": "2",
                    "MYRIAD_NUMBER_OF_CMX_SLICES": "2",
                }
                exec_net = ie.load_network(
                    network=net, device_name=(self.device_name), config=config
                )
            else:
                exec_net = ie.load_network(network=net, device_name=(self.device_name))
        else:
            exec_net = ie.import_network(
                (self.model_name), device_name=(self.device_name)
            )
        return exec_net

    def _initialize_model_inputs(self):
        input_blob_name, mem_blob_in_name = list(self.Model.input_info)
        mem_shape = self.Model.input_info[mem_blob_in_name].input_data.shape
        self.mem_blob_in_name = mem_blob_in_name
        self.mem_blob_in_tensor = np.zeros(mem_shape, dtype=(np.float16))
        self.input_blob_name = input_blob_name

    def _initialize_model_outputs(self):
        out_blob, mem_blob_out = list(self.Model.outputs.keys())
        self.out_blob_name = out_blob
        self.mem_blob_out_name = mem_blob_out

    def _get_preprocessor(self):
        n_batch, n_channels, model_h, model_w = self.Model.input_info[
            self.input_blob_name
        ].input_data.shape
        preprocessor = ProcessYUYVInput(model_w, model_h, channels=n_channels)
        return preprocessor

    def _get_postprocessor(self):
        postprocessor = ProcessOutput()
        return postprocessor

    def __call__(self, full_yuv_img, *args):
        yuv_img = self.pre_procesor(full_yuv_img.copy())
        if len(args) != 0:
            if args[0] != None:
                self.mem_blob_tensor = args[0]
        input_dict = {
            self.input_blob_name: yuv_img,
            self.mem_blob_in_name: self.mem_blob_in_tensor,
        }
        model_output = self.Model.infer(inputs=input_dict)
        self.mem_blob_in_tensor = model_output[self.mem_blob_out_name]
        mask = model_output[self.out_blob_name]
        mask = self.post_processor(mask, full_yuv_img)
        return mask
