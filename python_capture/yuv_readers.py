def yuv422torgb(yuv422_frame):
    Y = yuv422_frame[:, :, 0:1]
    U = yuv422_frame[:, 0::2, 1:2]
    V = yuv422_frame[:, 1::2, 1:2]
    U_full = np.zeros_like(Y)
    V_full = np.zeros_like(Y)
    # repeat to match size
    U_full[:, 0::2, :] = U
    U_full[:, 1::2, :] = U
    V_full[:, 0::2, :] = V
    V_full[:, 1::2, :] = V
    V = V_full
    U = U_full
    # YUV = np.dstack((Y, U, V))[:height, :width, :].astype(np.float)
    YUV = np.dstack((Y, U, V)).astype(np.float)
    YUV[:, :, 0] = YUV[:, :, 0] - 16   # Offset Y by 16
    YUV[:, :, 1:] = YUV[:, :, 1:] - 128  # Offset UV by 128
    # YUV conversion matrix from ITU-R BT.601 version (SDTV)
    #              Y       U       V
    M_SD = np.array([[1.164,  0.000,  1.596],    # R
                  [1.164, -0.392, -0.813],    # G
                  [1.164,  2.017,  0.000]])   # B
    
    M_HD = np.array([[1.164,     0.,  1.793],
                    [1.164, -0.213, -0.533],
                    [1.164,  2.112,     0.]])
    # Take the dot product with the matrix to produce RGB output, clamp the
    # results to byte range and convert to bytes
    RGB = YUV.dot(M_HD.T).clip(0, 255).astype(np.uint8)
    return RGB

class YUV422ReaderAV:
    def __init__(self, file_name, width, height, fps=30):
        self.options = {'video_size': '%sx%s' % (width, height),
                       'framerate': '%s' % round(fps),
                       'pixel_format': 'yuyv422'}
        self.container = av.open(file_name, 'r', options=self.options)
        self.gen = self.read_generator()
    
    def read_generator(self):
        for frame in self.container.decode():
            frame = frame.to_ndarray()
            yield frame
    
    def read(self):
        try:
            frame = next(self.gen)
            success = True
        except StopIteration:
            frame = None
            success = False
        return success, frame
    
    def skip_forward(self, num_frames):
        for i in range(num_frames):
            s, f = self.read()
        return s, f
    

class YUV422ReaderCV:
    def __init__(self, filename, width, height):
        self.height, self.width = height, width
        self.frame_len = self.width * self.height * 2
        self.cap = open(filename, 'rb')
        self.shape = (self.height, self.width, 2)
    
    def read(self):
        try:
            raw = self.cap.read(self.frame_len)
            if len(raw) == 0: return False, None
            frame = np.frombuffer(raw, dtype=np.uint8)
            frame = frame.reshape(self.height, self.width)
            return True, frame
        except Exception as e:
            print(str(e))
            return False, None

    def read_rgb(self):
        try:
            raw = self.f.read(self.frame_len)
            if len(raw) == 0: return False, None
            yuv = np.frombuffer(raw, dtype=np.uint8)
            yuv = yuv.reshape(self.shape)
#             bgr = cv2.cvtColor(yuv, cv2.COLOR_YUV2BGR_YUY2)
            rgb = cv2.cvtColor(yuv, cv2.COLOR_YUV2RGB_YUY2)
        except Exception as e:
            print(str(e))
            return False, None
        return True, rgb


class AVVideoWriter:
    def __init__(self, path, width, height, fps=14.98, do_overlay_mask=False):
        if os.path.exists(path):
            os.remove(path)
        self.container = av.open(path, mode='w')
        self.stream = self.container.add_stream('h264', rate=round(fps))
        self.do_mask_overlay = do_overlay_mask
        self.stream.width = width
        self.stream.height = height
        # self.stream.pix_fmt = 'rgb24'
        # self.stream.bit_rate = bit_rate

    def normalize(self, I):
        max_val = np.max(I)
        if max_val < 1e-5:
            max_val = 1
        I = (I/max_val) * 255
        I = np.uint8(I)
        return I

    def get_mask_overlayed_img(self, rgb_img, msk):
        msk = np.squeeze(msk)
        if len(msk.shape) == 2:
            msk = np.expand_dims(msk, axis=2)
        assert len(msk.shape) == 3
        if msk.shape[-1] == 1:
            msk = np.repeat(msk, 3, axis=-1)
        msk = self.normalize(msk)
        ht, wd, c = rgb_img.shape
        msk = cv2.resize(msk, (wd, ht))
        overlay_img = np.zeros_like(rgb_img)
        overlay_img[:, :, 1] = msk  # green color
        transparency = 0.5
        beta = 1
        gamma = 0
        overlay_img = cv2.addWeighted(overlay_img,
                                      transparency,
                                      rgb_img,
                                      beta,
                                      gamma)
        return overlay_img

    def write(self, frame, mask=None):
        # frames: [H, W, C]
        if mask is not None and self.do_mask_overlay:
            frame = self.get_mask_overlayed_img(frame, mask)
        # frame = av.VideoFrame.from_ndarray(frame, format='rgb24')
        frame = av.VideoFrame.from_ndarray(frame)
        self.container.mux(self.stream.encode(frame))

    def close(self):
        self.container.mux(self.stream.encode())
        self.container.close()

            
def test_first_frame_yuv422Reader():
    fname = "11-Jan-2022-Haris/11-Jan-2022-10_57_09.yuv"
    cap = YUV422ReaderAV(fname, 1280, 720)
    # cap = YUV422ReaderCV(fname, 1280, 720)
    s, f = cap.read()
    rgb = yuv422torgb(f)
    plt.imshow(rgb)
    plt.show()
    # cnt = 0
    # while s:
    #     cnt += 1
    #     s, f = cap.read()
    # print(cnt)
    
    
test_first_frame_yuv422Reader()