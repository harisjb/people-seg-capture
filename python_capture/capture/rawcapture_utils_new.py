import numpy as np


def yuv422torgb(yuv422_frame):
    Y = yuv422_frame[:, :, 0:1]
    U = yuv422_frame[:, 0::2, 1:2]
    V = yuv422_frame[:, 1::2, 1:2]
    U_full = Y * 0
    V_full = Y * 0
    U_full[:, 0::2, :] = U
    U_full[:, 1::2, :] = U
    V_full[:, 0::2, :] = V
    V_full[:, 1::2, :] = V
    V = V_full
    U = U_full
    YUV = np.dstack((Y, U, V)).astype(np.float)
    YUV[:, :, 0] = YUV[:, :, 0] - 16
    YUV[:, :, 1:] = YUV[:, :, 1:] - 128
    M = np.array([[1.164, 0.0, 1.596], [1.164, -0.392, -0.813], [1.164, 2.017, 0.0]])
    RGB = YUV.dot(M.T).clip(0, 255).astype(np.uint8)
    return RGB
