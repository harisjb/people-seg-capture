import cv2, numpy as np


def overlay(mask, rgb_img):
    assert len(mask.shape) == 2
    if np.max(mask) > 1e-05:
        mask = mask / np.max(mask) * 255
    mask = mask.astype(np.uint8)
    overlay_img = np.zeros_like(rgb_img)
    overlay_img[:, :, 1] = mask
    transparency = 0.5
    beta = 1
    gamma = 0
    overlay_img = cv2.addWeighted(overlay_img, transparency, rgb_img, beta, gamma)
    return overlay_img
